import java.util.Scanner;
public class Konsolenausgabe_Aufgabe2 {

	public static void main(String[] args) {
		
		/*
		 * 
		 * Bitte keine Baumgr��e von 1 verwenden!
		 * 
		 */
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Wie gro� soll dein Baum werden?");
		
		int sternZeichen; //inkrementeren um 2 pro Schritt;
		int baumGroesse =  scan.nextInt() + 1; //Gr��e wird um 1 inkrementiert, da ansonsten eine Fehlerexception geworfen wird.
	
		
		
		//Von Punkt 1 (da Punkt 0 einen Fehler ausl�st) durchl�uft die Schleife bis zur Endgr��e vom Baum
		for(int i = 1; i < baumGroesse; i++) {
			if(i==1) {
				sternZeichen = 1;
			} else {
				sternZeichen = 1 + 2*(i-1);
			}

			System.out.printf("%"+((i-baumGroesse))+"s", "" ); //Ausgabe der leeren Stelle.

			for(int z = sternZeichen; z>0; z--) {
				
				System.out.print("*"); //Die berechnete Anzahl an Sterne wird ausgegeben.
			}
			System.out.println("");
		}
		
		sternZeichen = (2*(baumGroesse-2))/2; //Hier wird die Mitte des Baumes berechnet.
		for(int i = 0; i < 2; i++) {
		System.out.printf("%"+(sternZeichen)+"s", "" );
			for(int z = 0; z < 3; z++) {
				System.out.print("*");
			}
			System.out.println("");
		}
	
	}

}
