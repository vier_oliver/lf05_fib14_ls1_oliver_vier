
public class Konsolenausgabe_Aufgabe1 {

	public static void main(String[] args) {

		//Beispielsatz ohne Formatierung.
		System.out.println("Ich mag Computer. Zusätzlich auch Eis.");
		
		//Beispielsatz mit Zeilenumbruch.
		System.out.println("Ich mag Computer.\nZusätzlich auch Eis.");
		
		//Beispielsatz mit Zeilenumbruch, hinzugefügt an einer String-Variable
		String ausgabe = "Ich mag Computer.\n";
		ausgabe += "Zusätzlich auch Eis.";
		System.out.println(ausgabe);
		
		//Hier sieht man den Unterschied zwischen print und println. Es werden keine zusätzlichen Zeilenumbrüche hinzugefügt.
		
		
		System.out.println("\n"); //Zusätzlicher Zeilenumbruch nur für visuelle Zwecke.
		
		System.out.print("Ich mag Computer. Zusätzlich auch Eis.");
		System.out.print("Ich mag Computer. Zusätzlich auch Eis.");
		
		System.out.println("\n"); //Zusätzlicher Zeilenumbruch nur für visuelle Zwecke.
		
		System.out.println("Ich mag Computer. Zusätzlich auch Eis.");
		System.out.println("Ich mag Computer. Zusätzlich auch Eis.");
		
		//Hier füge ich jetzt mit Hilfe von printf Anführungszeichen am Anfang und Ende des Satzes ein.
		//printf bietet die Möglichkeit, mit bestimmten Charakteren bestimmte Formatierung zu verwenden. 
		
		System.out.printf( "\"%s\"","Ich mag Computer. Zusätzlich auch Eis.");
		
		//Jetzt auch noch mit Verkettungsoperator. Somit wird das Format auf beide String angewendet.
			
		System.out.printf( "\"%s\"","Ich mag Computer" + "Zusätzlich auch Eis.");
		
		//Und hier ohne Format
		System.out.println("\nIch mag Computer." + " Zusätzlich auch Eis.");
		
	}

}
