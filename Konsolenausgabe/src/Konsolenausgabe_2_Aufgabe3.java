
public class Konsolenausgabe_2_Aufgabe3 {

	public static void main(String[] args) {
		
		double valueFahr1 = -20;
		double valueFahr2 = -10;
		double valueFahr3 = 0;
		double valueFahr4 = 20;
		double valueFahr5 = 30;
		
		double[] valueFahrList = {valueFahr1, valueFahr2, valueFahr3, valueFahr4, valueFahr5};
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("|");
		System.out.printf("%10s", "Celsius");
		
		System.out.println("\n-----------------------");
		
		for(int i = 0; valueFahrList.length > i; i++) {
			System.out.printf("%-12.0f", valueFahrList[i]);
			System.out.printf("|");
			double celsius = ((valueFahrList[i]-32.0)*(5.0/9.0));
			System.out.printf("%10.2f\n", celsius);
		}
		

	}

}
