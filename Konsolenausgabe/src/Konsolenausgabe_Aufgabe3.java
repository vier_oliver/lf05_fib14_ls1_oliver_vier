
public class Konsolenausgabe_Aufgabe3 {

	public static void main(String[] args) {

		double value1 = 22.4234234;
		double value2 = 111.2222;
		double value3 = 4.0;
		double value4 = 1000000.551;
		double value5 = 97.34;
		
		//Werte werden in der valueList abgespeichert.
		double[] valueList = {value1,value2,value3,value4,value5};

		for(int i = 0; i < valueList.length; i++) { //Die Liste wird dann hier mit Hilfe der Schleife ausgegeben.
			System.out.printf( "%.2f\n" , valueList[i]); 
		}
		
	}

}
