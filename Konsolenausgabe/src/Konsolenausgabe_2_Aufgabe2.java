import java.util.Scanner;
public class Konsolenausgabe_2_Aufgabe2 {

	public static void main(String[] args) {

		System.out.println("Welche Fakult�t soll verwendet werden?");
		Scanner scan = new Scanner(System.in);
		int laenge = scan.nextInt();
		int zahl=1;
		int zeilenAbstand = 4; //Grunds�tzlicher Zeilenabstand.
		
		zeilenAbstand = (zeilenAbstand*laenge)-1; //Hart-gecodete 0, da im unteren Code eine Exception auftreten w�rde.
		System.out.printf("0!");
		System.out.printf("%4s", "=");
		System.out.printf("%"+zeilenAbstand+"s=   1\n", " ");
		zeilenAbstand = 4;
		
		//Allgemeine Information: Bis 100 sind die Eingaben richtg formatiert. Bis 33 gibt es eine Zahlenausgabe.
		
		for(int i = 1; i <= laenge; i++) {
			System.out.printf( "%-5s=", (i+"!"));
			for(int z = 1; z <= laenge-(laenge-i); z++ ) {
				
				if(z == laenge-(laenge-i)) {
					zeilenAbstand = (zeilenAbstand*laenge)+3 - ((zeilenAbstand*z)+1); //+3 steht f�r die Rechts-Verschiebung. Die +1 am Ende wird verwendet, damit keine Exception geworfen wird.
					System.out.printf("%2s", z);
					System.out.printf("%"+zeilenAbstand+"s", " =");
					System.out.printf("%4d", zahl);
					zahl=1;
					zeilenAbstand = 4;
				} else {
					zahl*=z+1; //Allgemeine Zahlenausgabe und Formatierung, wenn die letzte Zahl noch nicht erreicht wurde.
					System.out.printf("%2d *", z);
				}
				
				
			}
			System.out.println(""); //Zeilenumbruch.
				
		}
		
		
		
	}

}
