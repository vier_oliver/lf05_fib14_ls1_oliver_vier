package Aufgabe_2;

public class Sparbuch {
	
	private int kontonummer;
	private float kapital;
	private float zinssatz;

	public Sparbuch(int kontonummer, float kapital, float zinssatz) {
		this.kontonummer = kontonummer;
		this.kapital = kapital;
		this.zinssatz = zinssatz;
	}
	
	public void zahleEin(float einzuzahlenderBetrag) {
		kapital += einzuzahlenderBetrag;
	}

	public void hebeAb(float auszuzahlenderBetrag) {
		kapital -= auszuzahlenderBetrag;
	}
	
	public float getErtrag(int laufzeitJahre) {
		
		float ertrag = kapital;
		for(int i = 0; i < laufzeitJahre; i++) {
			ertrag += ertrag*(zinssatz/100);
		}
		
		return ertrag;
	}
	
	public void verzinse() {
		kapital += kapital*(zinssatz/100);
	}

	public int getKontonummer() {
		return kontonummer;
	}

	public float getKapital() {
		return kapital;
	}

	public float getZinssatz() {
		return zinssatz;
	}

}
