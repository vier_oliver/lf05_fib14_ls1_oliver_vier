import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Jahr eingeben: ");
		int schaltjahr = scan.nextInt();
		
		if((schaltjahr%4==0) && (!(schaltjahr%100==0) || schaltjahr%400 == 0)) {
			System.out.println(schaltjahr + " ist ein Schaltjahr.");
		} else {
			System.out.println(schaltjahr + " ist kein Schaltjahr.");
		}

	}

}
