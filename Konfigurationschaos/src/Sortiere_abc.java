import java.util.Scanner;
public class Sortiere_abc {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		char zeichen1 = ' ', zeichen2 = ' ', zeichen3 = ' ';
		char[] zeichenListe = {zeichen1, zeichen2, zeichen3};
		
		for(int i = 0; i < zeichenListe.length; i++) {
			System.out.println((i+1)+".) Gebe bitte ein Zeichen ein: ");
			zeichenListe[i] = scan.next().charAt(0);
		}
		
		//Einfaches Vergleichen der Char-Werte mit Hilfe der UTF-Position
		
		for(int i = 0; i < zeichenListe.length; i++ ) {
			System.out.println((i+1)+".) " + ((int)zeichenListe[i]));
		}
		
		for(int i = 0; i < zeichenListe.length-1; i++) {
			for(int z = i+1; z<zeichenListe.length; z++) {
				if((int)zeichenListe[i] > (int)zeichenListe[z]) {
					char storeVar = zeichenListe[i];
					zeichenListe[i] = zeichenListe[z];
					zeichenListe[z] = storeVar;
					i = 0;
				}
			}
		}
		
		for(int i = 0; i < zeichenListe.length; i++ ) {
			
			System.out.println((i+1)+".) In Zahlen: " + ((int)zeichenListe[i]) + " als Wert: " + zeichenListe[i]);
		}
		
	}

}