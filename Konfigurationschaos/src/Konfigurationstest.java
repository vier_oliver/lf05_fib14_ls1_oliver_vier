
public class Konfigurationstest {

	public static void main(String[] args) {
		
		//Aufgabe 1
		int cent = 70;
		cent = 80;
		
		double maximum = 95.50;
		
		//Aufgabe 2
		boolean state = true;
		short zahl = -1000;
		float kommazahl = 1.255f;
		char character = '#';
		
		//Aufgabe 3
		
		String satz = "Schade, nur drei";
		
		final short check_nr; //Werden Konstanten nicht gro� geschrieben?
		check_nr = 8765;
		//Aufgabe 4
		
		/*Die Verwendung von Datentypen ist sinnvoll, da man somit die Speichernutzung selbstst�ndig bestimmen kann.
		 * Zus�tzlich erh�ht man somit die Lesbarkeit. Beispielsweise wenn man Variablen nur mit "var" wie in JavaScript bezeichnen k�nnte,
		 * w�re es m�glicherweise schwieriger f�r Anf�nger, die Sprache zu lernen.
		 */
		
		
		
		/*
		 * Operatoren
		 */
		
		//Aufgabe 1
		
		int ergebnis = 4+8*9-1;
		System.out.println(ergebnis);
		
		int zaehler = 1;
		zaehler++;
		System.out.println(zaehler);
		
		System.out.println("Ganzzahldivision: " + (22/6));
		
		//Aufgabe 2
		
		int schalter = 10;
		if(schalter > 7 && schalter < 12) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		if(schalter != 10 || schalter == 12) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		//Aufgabe 3
		
		String zeichenkette = "Meine Oma " + "f�hrt im " + "H�hnerstall Motorrad";
		System.out.println(zeichenkette);
		
	}

}
