import java.util.Scanner;
public class Begruessung {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Guten Morgen, ich habe ein paar Fragen an Sie.\n");
		
		System.out.println("Wie lautet ihr Name?");
		String name = scan.next();
		System.out.println("Wie alt sind sie?");
		int alter = scan.nextInt();
		
		System.out.printf("Also verstehe ich richtig? Sie sind Herr/Frau %s und sind %d Jahre alt",name, alter);
		
		scan.close();
	}

}
