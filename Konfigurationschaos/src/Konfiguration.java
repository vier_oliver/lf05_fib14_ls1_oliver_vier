public class Konfiguration{
	public static void main(String [] args) {
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name;
		int summe;
		int euro;
		int cent;
		double patrone = 46.24;
		char sprachModul = 'd';
		boolean status;

		final byte PRUEFNR = 4;

		double prozent;
		double maximum = 100.00;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		name = typ + " " + bezeichnung;
		
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;

		prozent = maximum - patrone;
		status = (euro <= 150) &&  (!(PRUEFNR == 5 || PRUEFNR == 6) && (prozent >= 50.00) && (euro >= 50) && (cent != 0) && (sprachModul == 'd'));   
		
		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Prüfnummer : " + PRUEFNR);
		System.out.println("Füllstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + status);
		
		
		
	}
}


