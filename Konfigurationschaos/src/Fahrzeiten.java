import java.util.Scanner;
public class Fahrzeiten {

	public static void main(String[] args) {

		
		Scanner scan = new Scanner(System.in);
		
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'n';
		
		System.out.println("Wohin m�chtest du fahren? (h-Hannover, w-Wolfsburg, b-Braunschweig");
		char endetIn = scan.next().charAt(0);
		
		fahrzeit += 8;
		
		//Wenn richtungHamburg gleich 'j' ist, dann wird Hamburg auch als Ziel ausgegeben.
		//Andernfalls werden die anderen Abfragen ausgef�hrt.
		//Beim Zwischenhalt wird die Ausgabe sofort ausgegeben, da man den Halt nicht mit einberechnet.
		
		//Wenn kein Ziel genau eingegeben wurde und richtungHamburg 'n' ist, dann wird "Ziel unbekannt" ausgegeben.
		
		if(haltInSpandau == 'j') {
			System.out.println("Zwischenhalt in Spandau nach " + fahrzeit + " Minuten.");
			fahrzeit += 2;
		}
		
		if(richtungHamburg == 'j') {
			fahrzeit += 96;
			System.out.println("Sie erreichen Hamburg HBF nach " + fahrzeit + " Minuten.");
		} else {
			fahrzeit += 34;
		
		
		if(haltInStendal == 'j') {
			System.out.println("Zwischenhalt in Stendal nach " + fahrzeit + " Minuten.");
			fahrzeit += 16;
		} else {
			fahrzeit += 6; 
		}
		
		if(endetIn == 'h') {
			fahrzeit += 63;
			System.out.println("Sie erreichen Hannover nach " + fahrzeit + " Minuten.");
		} else if(endetIn == 'b'){
			fahrzeit += 50;
			System.out.println("Sie erreichen Braunschweig nach " + fahrzeit + " Minuten.");
		} else if(endetIn == 'w'){
			fahrzeit += 29;
			System.out.println("Sie erreichen Wolfsburg nach " + fahrzeit + " Minuten.");
		} else {
			System.out.println("Ziel unbekannt.");
		}
		
		//Hamburg HBF k�nnte als gro�es H dargestellt werden.
		
		}
		
	}

}
