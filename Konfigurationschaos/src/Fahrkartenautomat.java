import java.util.ArrayList;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       //Die restlichen Variablen werden zum zwischenspeichern verwendet.
       
       Scanner tastatur = new Scanner(System.in);
       
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();

       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben();

       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	   
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	//Aufgabe A5.3.1 Welche Vorteile hat man durch das Erstellen von Arrays?
    	
    	/*
    	 *Die Vorteile liegen darin, dass man einen zentralen Speicherort hat. Ver�nderungen von Daten
    	 *erfolgen nur an einer Stelle. Das bedeutet, dass mehrere Ungleichheiten an Daten im Programm weniger
    	 *wahrscheinlich auftreten. Durch diesen Schritt wird somit auch die Wiederverwendbarket erh�ht.
    	 */
    	
    	//Aufgabe A5.3.3
    	
    	/*
    	 * Die Vorteile darin liegen, da alles zentral an einen Ort ge�ndert werden kann.
    	 * Somit k�nnen sofort �berall im Programm �nderungen vorgenommen werden. Das sorgt f�r eine erh�hte Sicherheit
    	 * das alle Daten gleich sind und keine Unterschiede auftreten. Zudem wird auch die Wiederverwendbarket erh�ht.
    	 * Zudem wird die Problemfindung vereinfacht.
    	 * 
    	 * Die Vorteile sind auch teilweise als Nachteile vertreten. Sollten falsche Daten an einer Stelle eingetragen werden,
    	 * k�nnten sie �berall im Programm falsch auftreten. Zudem wird mehr Speicher verwendet. Das wird aber bei dieser Menge an Daten keinen
    	 * besonderen Unterschied machen.
    	 * 
    	 * Vorher wurden die Daten manuell eingetragen. Dort konnten sehr viele Daten falsch eingetragen werden.
    	 * Jetzt ist alles zentralisiert. Somit besteht eine h�here Sicherheit der Datenrichtigkeit. 
    	 */
    	
    	//Aufgabe A.5.4.1
    	
    	/*
    	 * Aufgabenstellung von Moodle: https://moodle.oszimt.de/mod/page/view.php?id=279040 "Hinweis f�r Verbesserungen" (https://moodle.oszimt.de/mod/page/view.php?id=279040) Zugriff: 31.01.2022 10:32 Uhr
    	 * 
    	 * <x> Fahrkarteninformationen sollen innerhalb des Quelltextes an einer Stelle (Methode) verwaltet werden. -> sie werden bereits an einer Stelle verwaltet (bearbeitet).
    	 * <x> Ung�ltige Geldbetr�ge werden beim Einwerfen erkannt und es erfolgt eine angemessene Reaktion. -> erledigt.
    	 * <x> Methoden und Variablen werden konsistent benannt. -> sind schon l�ngst konsistent benannt.
    	 * <x> Die Interaktion mit dem Benutzer wird optimiert. -> Textnachrichten wurden benutzerfreundlicher geschrieben.
    	 * 
    	 */
    	
    	String[] fahrkartenBezeichnungListe = {
    			
    			"Einzelfahrschein Berlin AB",
    			"Einzelfahrschein Berlin BC",
    			"Einzelfahrschein Berlin ABC",
    			"Kurzstrecke",
    			"Tageskarte Berlin AB",
    			"Tageskarte Berlin BC",
    			"Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB",
    			"Kleingruppen-Tageskarte Berlin BC",
    			"Kleingruppen-Tageskarte Berlin ABC"	
    	
    	};
    	
    	double[] fahrkartenPreiseListe = {
    			
    			2.90,
    			3.30,
    			3.60,
    			1.90,
    			8.60,
    			9.00,
    			9.60,
    			23.50,
    			24.30,
    			24.90
    			
    	};
    	
    	
    	double zuZahlenderBetrag = 0;
        double ticketEinzelpreis = 0;
        int ticketAnzahl;
        
        short caseWert;
        boolean caseBool = false;

        System.out.println("Fahrkartenbestellvorgang:\n=========================\n");
        System.out.println("Guten Tag, hier k�nnen Sie sich eine Fahrkarte kaufen.\n");
        
        while(caseBool == false) {
        	
        	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
        	
        	for(int i = 0; i < fahrkartenBezeichnungListe.length; i++) {
        		System.out.printf("  %s [%.2f EUR] (%d)\n", fahrkartenBezeichnungListe[i], fahrkartenPreiseListe[i], i+1);
        	}
        	
        	System.out.printf("  Bezahlen(%d)\n", fahrkartenBezeichnungListe.length+1);
        	System.out.print("\nIhre Auswahl: ");
        	caseWert = tastatur.nextShort();
        
        	if(caseWert > 0 && caseWert <= fahrkartenBezeichnungListe.length ) {
        	
        		System.out.print("Wie viele Tickets m�chten Sie kaufen? (1-10) ");
        		ticketAnzahl = tastatur.nextInt();
        		while(ticketAnzahl <= 0 || ticketAnzahl > 10) {
        			System.out.println(">> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        			ticketAnzahl = tastatur.nextInt();
		    
        	}
		    
		    caseBool = false;
			ticketEinzelpreis = fahrkartenPreiseListe[caseWert-1];
			zuZahlenderBetrag += ticketEinzelpreis*ticketAnzahl;
			
			System.out.printf("\nZwischensumme: %.2f �\n\n",zuZahlenderBetrag);
			
        	} else if( caseWert == fahrkartenBezeichnungListe.length+1) {
        		caseBool = true;
        	} 
        
       		else {
        		System.out.println("\n>>Falsche Eingabe<<\n");
        	}
        }

        return zuZahlenderBetrag;
        
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag;
        double eingeworfeneM�nze;
        double[] allowedValues = {0.05, 0.1, 0.2, 0.5, 1, 2};
        
    	// Geldeinwurf
        // -----------
        
        boolean correctValues = false;
        
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
     	   
     	   //Checkt, ob der Spieler einen Wert innerhalb des Bereiches eingibt.
     	   if(eingeworfeneM�nze <= 2 && eingeworfeneM�nze >= 0.05 ) {
     		   
     		   //Checkt, ob eine zugelassene M�nze verwendet wird -> (contains(Number)).
     		   for (double num : allowedValues) {
				
     			   if(eingeworfeneM�nze == num) {
     				   correctValues = true;
     			   }
     		   }
     	   }
     	   
     	   if(correctValues == true) {eingezahlterGesamtbetrag += eingeworfeneM�nze;}
     	   else {System.out.println("\nDu musst g�ltige M�nzen einwerfen!\n"); }
     	   
           
           correctValues = false;
        }
        
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
    	
        System.out.println("\nDer/Die Fahrschein/e wird/werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        
    	double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	
    	ArrayList<Double> r�ckgabebetragListe = new ArrayList<Double>();
    	ArrayList<String> einheitListe = new ArrayList<String>();

        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	r�ckgabebetragListe.add(2.0);
              	einheitListe.add("EURO");
              	
 	          	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	r�ckgabebetragListe.add(1.0);
            	einheitListe.add("EURO");	
            	
            	r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	r�ckgabebetragListe.add(50.0);
            	einheitListe.add("CENT");
            	
            	r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	r�ckgabebetragListe.add(20.0);
            	einheitListe.add("CENT");
            	
  	          	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	r�ckgabebetragListe.add(10.0);
            	einheitListe.add("CENT");
            	
            	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	r�ckgabebetragListe.add(5.0);
            	einheitListe.add("CENT");
            	
  	          	r�ckgabebetrag -= 0.05;
            }
        }

        
        muenzeAusgeben(r�ckgabebetragListe, einheitListe);
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n");	
    }
    
    public static void warte(int millisekunden){
    	try {
 			Thread.sleep(millisekunden);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void muenzeAusgeben(ArrayList<Double> r�ckgabebetragListe, ArrayList<String> einheitListe) {

    	int counter = 0;
    	int counterEnd = 0;
    	boolean breakLoop = false;
    	
    	while(true) {
    		
    		if(!(counter+3 > r�ckgabebetragListe.size()-1)) {
    			
    			counterEnd = counter + 3;
    			
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %4s * %-4s", "*", "*");
    			}
    			System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %2s %5s %-2s", "*", " ", "*");
    			}
    			System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    		    	System.out.printf(" %1s %5.0f %3s", "*", r�ckgabebetragListe.get(i) ,"*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    		        System.out.printf(" %s %6s %2s", "*", einheitListe.get(i), "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %2s %5s %-2s", "*", " ", "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %4s * %-4s", "*", "*");
    			}System.out.println("");
    			
    			counter += 3;
    			
    		} else {
    			
    			counterEnd = r�ckgabebetragListe.size();
    			
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %4s * %-4s", "*", "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %2s %5s %-2s", "*", " ", "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    		    	System.out.printf(" %1s %5.0f %3s", "*", r�ckgabebetragListe.get(i) ,"*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    		        System.out.printf(" %s %6s %2s", "*", einheitListe.get(i), "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %2s %5s %-2s", "*", " ", "*");
    			}System.out.println("");
    			for(int i = counter; i < counterEnd; i++) {
    				System.out.printf(" %4s * %-4s", "*", "*");
    			}System.out.println("");
    			
    			break;
    			
    		}
    		
    		
    		
    	}


    	

	   	
    	/*    * * *	     
    	 *  *       *
    	 * *    2    *
    	 * *   EURO	 * 
    	 *	*	    *
    	 * 	  * * *
    	 */
    	
    	/*    * * *       * * *
    	 *  *	    *   *       *
    	 * *    2    * *    2    *
    	 * *   EURO	 * *   EURO  *
    	 *	*	    *   *       *
    	 * 	  * * *       * * *
    	 */
    	
    }
    
}

//Aufgabe 5 -> Ich habe f�r die Variable ticketEinzelpreis den Datentyp float gew�hlt, da wir Kommazahlen
//verwenden. Zudem speichern wir nicht sehr gro�e Zahlen & Nachkommastellen ab, sodass wir kein Double ben�tigen. 

/*Die Variable ticketAnzahl hat den Datentyp short bekommen, da keiner so viele Tickets kaufen w�rde, sodass wir 16 Bits f�rs Abspeichern dieser ben�tigen.
 * Zudem spart man sich somit Speicherplatz. Short kann genauso wie der Integer positive und negative Zahlen abspeichern.
 */

//Aufgabe 6: Da unterschiedliche Datentypen verwendet werden, muss man darauf achten, wie diese zusammengerechnet werden.
//Ich sch�tze, dass diese so gecastet werden, dass sie miteinander addiert werden k�nnen. Vermutlicherweise wird
//short als float gecastet, sodass letztendlich auch Kommazahlen verwendet werden k�nnen. Daraufhin werden die Werte zusammengerechnet und in zuZahlenderBetrag gespeichert.