public class Brot extends Speise {

	public Brot(String name, int menge) {
		super(name, menge);
	}
	
	public Brot(int nummer, int menge) {
		super("", menge);
		switch (nummer) {
		case 1: {
			this.name = "Schwarzbrot";
			break;
		}
		case 2: {
			this.name = "Mischbrot";
			break;
		}
		case 3: {
			this.name = "Spezialbrot";
			break;
		}
		default:
		}
	}
	
	@Override
	public boolean essen(){
		
		this.menge -= 50;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}
	
	public boolean essen(int menge){
		
		this.menge -= menge;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}


}
