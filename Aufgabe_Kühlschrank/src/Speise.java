
public class Speise extends Lebensmittel{

	public Speise(String name, int menge) {
		super(name, menge);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean essen() {
		return true;
	}
	
	//Always false
	@Override
	public boolean trinken() {
		return false;
	}
	
	@Override
	public String status() {
		return "Klasse: " + getClass().getName() + ", Instanz: " + this.name + ", Menge: " + this.menge + "g";
	}

}
