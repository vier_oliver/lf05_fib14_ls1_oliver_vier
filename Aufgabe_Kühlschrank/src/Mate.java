
public class Mate extends Getraenk{

	public Mate(String name, int menge) {
		super(name, menge);
	}
	
	public Mate(String name) {
		super(name, 500);
	}
	@Override
	public boolean trinken(){
		
		this.menge -= 100;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}

	public boolean trinken(int menge){
		
		this.menge -= menge;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}
}
