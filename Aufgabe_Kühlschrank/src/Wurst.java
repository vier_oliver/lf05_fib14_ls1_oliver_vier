
public class Wurst extends Speise {

	public Wurst(String name, int menge) {
		super(name, menge);
	}

	@Override
	public boolean essen(){
		
		this.menge -= 10;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}
	
	public boolean essen(int menge){
		
		this.menge -= menge;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}


}
