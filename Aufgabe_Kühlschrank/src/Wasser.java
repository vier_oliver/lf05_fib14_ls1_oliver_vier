
public class Wasser extends Getraenk{

	public Wasser(String name, int menge) {
		super(name, menge);
	}

	@Override
	public boolean trinken(){
		
		this.menge -= 200;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}

	public boolean trinken(int menge){
		
		this.menge -= menge;		
		if(this.menge < 0) {
			this.menge = 0;
			return false;
		}
		return true;
	}
	
}
