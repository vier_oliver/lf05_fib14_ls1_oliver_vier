
public class Getraenk extends Lebensmittel{


	public Getraenk(String name, int menge) {
		super(name, menge);
	}

	//Always false
	@Override
	public boolean essen() {
		return false;
	}

	@Override
	public boolean trinken() {
		return true;
	}

	@Override
	public String status() {
		return "Klasse: " + getClass().getName() + ", Instanz: " + this.name + ", Menge: " + this.menge + "ml";
	}

}
