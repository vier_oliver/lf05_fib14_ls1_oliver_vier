import java.util.ArrayList;

public class Raumschiff {

	private String name;

	private int energieversorgung;

	private int huelle;

	private int schutzschild;

	private int lebenserhaltungssysteme;

	private int anzahlReparaturAndroiden;

	private int anzahlPhotonentorpedos;

	// Ich verwende zur Einfachkeit eine ArrayList, statt ein normales Array.
	private ArrayList<Ladung> ladungsverzeichnis;

	// Statt ein statisches Attribut verwende ich hier eine private ArrayList,
	// welche von der Main-Methode der Testklasse übergeben wird.
	// Jedes Raumschiff kann darauf zugreifen und ich muss nicht "static" verwenden.
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	public Raumschiff() {

	}

	// Der BroadcastKommunikator wird hier nicht übergeben, da er oben schon
	// initialisiert wird.
	public Raumschiff(String name, int energieversorgung, int huelle, int schutzschild, int lebenserhaltungssysteme,
			int anzahlReparaturAndroiden, int anzahlPhotonentorpedos, ArrayList<Ladung> ladungsverzeichnis) {

		this.name = name;
		this.energieversorgung = energieversorgung;
		this.huelle = huelle;
		this.schutzschild = schutzschild;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.anzahlReparaturAndroiden = anzahlReparaturAndroiden;
		this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void torpedosSchießen(Raumschiff raumschiff) {
		if (getAnzahlPhotonentorpedos() == 0) {
			sendeBroadcast("-=*Click*=-");
		} else {
			setAnzahlPhotonentorpedos(getAnzahlPhotonentorpedos() - 1);
			sendeBroadcast("Photonentorpedo abgeschossen");
			raumschiff.schiffGetroffen();
		}
	}

	public void phaserkanonenSchießen(Raumschiff raumschiff) {
		if (getEnergieversorgung() < 50) {
			sendeBroadcast("-=*Click*=-");
		} else {
			setEnergieversorgung(getEnergieversorgung() - 50);
			sendeBroadcast("Phaserkanone abgeschossen");
			raumschiff.schiffGetroffen();
		}
	}

	private void schiffGetroffen() {

		sendeBroadcast(getName() + " wurde getroffen");

		if (schutzschild > 0) {
			schutzschild -= 50;
		} else if (huelle > 0) {
			huelle -= 50;
			energieversorgung -= 50;
		}

		if (huelle <= 0) {
			setLebenserhaltungssysteme(0);
			sendeBroadcast("Lebenserhaltungssysteme zerstört");
		}

	}

	public void sendeBroadcast(String nachricht) {
		System.out.println(nachricht);
		broadcastKommunikator.add(nachricht);
	}

	public void beladeRaumschiff(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
	}

	public static ArrayList<String> erhalteLogbucheintraege() {
		return broadcastKommunikator;
	}

	public void torpedosLaden(int anzahlTorpedosLaden) {

		boolean photonentorpedoVorhanden = false;

		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getName().equals("Photonentorpedo")) {
				photonentorpedoVorhanden = true;

				// Anzahl der zu ladenden Torpedos ist größer als möglich -> alle werden
				// geladen.

				if (anzahlTorpedosLaden > ladung.getAnzahl()) {
					setAnzahlPhotonentorpedos(getAnzahlPhotonentorpedos() + ladung.getAnzahl());
					System.out.println(ladung.getAnzahl() + " Photonentorpedo(s) eingesetzt.");
					// Alle wurden geladen = keine bleiben übrig.
					ladung.setAnzahl(0);

				}

				else {
					setAnzahlPhotonentorpedos(getAnzahlPhotonentorpedos() + anzahlTorpedosLaden);
					ladung.setAnzahl(ladung.getAnzahl() - anzahlTorpedosLaden);
					System.out.println(anzahlTorpedosLaden + " Photonentorpedo(s) eingesetzt.");
				}

				return;
			}
		}

		if (photonentorpedoVorhanden == false) {
			System.out.println("Keine Photonentorpedos gefunden!");
			sendeBroadcast("-=*Click*=-");
			return;
		}
	}

	public void sendeReparaturauftrag(boolean schildReparatur, boolean energieversorgungReparatur,
			boolean schiffshuelleReparatur, int anzahlReparaturAndroiden) {

		if (getAnzahlReparaturAndroiden() == 0) {
			System.out.println("Es sind keine Reparatur Androiden vorhanden");
			return;
		}

		int zuRepariendeModule = 0;

		if (schildReparatur == true)
			zuRepariendeModule += 1;
		if (energieversorgungReparatur == true)
			zuRepariendeModule += 1;
		if (schiffshuelleReparatur == true)
			zuRepariendeModule += 1;

		float reparaturWert = (anzahlReparaturAndroiden > getAnzahlReparaturAndroiden())
				? (float) (((Math.random() * 100) * getAnzahlReparaturAndroiden()) / zuRepariendeModule)
				: (float) (((Math.random() * 100) * anzahlReparaturAndroiden) / zuRepariendeModule);

		if (schildReparatur == true)
			setSchutzschild((int) (getSchutzschild() + reparaturWert));
		if (energieversorgungReparatur == true)
			setEnergieversorgung((int) (getEnergieversorgung() + reparaturWert));
		if (schiffshuelleReparatur == true)
			setHuelle((int) (getHuelle() + reparaturWert));

	}

	public void statusEinsehen() {

		System.out.println("________________________________");
		System.out.println("Name: " + getName());
		System.out.println("Energieversorgung (%): " + getEnergieversorgung());
		System.out.println("Huelle (%): " + getHuelle());
		System.out.println("Schutzschild (%): " + getSchutzschild());
		System.out.println("Lebenserhaltungssystem (%): " + getLebenserhaltungssysteme());
		System.out.println("Anzahl der Reparatur-Androiden: " + getAnzahlReparaturAndroiden());
		System.out.println("Anzahl der Photonentorpedos: " + getAnzahlPhotonentorpedos());
		System.out.println("Ladungsverzeichnis:");
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(" " + ladung);
		}
		System.out.println("Broadcast-Kommunikator:");
		for (String nachricht : broadcastKommunikator) {
			System.out.println(" " + nachricht);
		}
		System.out.println("________________________________");

	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladung: ");
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println("  " + ladung.toString());
		}
	}

	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getAnzahl() == 0) {
				ladungsverzeichnis.remove(ladungsverzeichnis.get(i));
			}
		}
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(int energieversorgng) {
		this.energieversorgung = energieversorgng;
	}

	public int getHuelle() {
		return huelle;
	}

	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	public int getSchutzschild() {
		return schutzschild;
	}

	public void setSchutzschild(int schuzschild) {
		this.schutzschild = schuzschild;
	}

	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	public int getAnzahlReparaturAndroiden() {
		return anzahlReparaturAndroiden;
	}

	public void setAnzahlReparaturAndroiden(int anzahlReparaturAndroiden) {
		this.anzahlReparaturAndroiden = anzahlReparaturAndroiden;
	}

	public int getAnzahlPhotonentorpedos() {
		return anzahlPhotonentorpedos;
	}

	public void setAnzahlPhotonentorpedos(int anzahlPhotonentorpedos) {
		this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikatorParameter) {
		broadcastKommunikator = broadcastKommunikatorParameter;
	}
	
}
