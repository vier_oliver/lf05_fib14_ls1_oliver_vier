import java.util.ArrayList;
import java.util.Arrays;

public class Testklasse {

	public static void main(String[] args) {

		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung batleth = new Ladung("Bat'leth Klingonen Schwert", 200);

		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung plasmawaffe = new Ladung("Plasma-Waffe", 50);

		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);

		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 2, 1,
				new ArrayList<Ladung>(Arrays.asList(schneckensaft, batleth)));
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2,
				new ArrayList<Ladung>(Arrays.asList(borgSchrott, roteMaterie, plasmawaffe)));
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 50, 80, 100, 5, 0,
				new ArrayList<Ladung>(Arrays.asList(forschungssonde, photonentorpedo)));

		// Ausführen des Spiels

		klingonen.torpedosSchießen(romulaner);
		System.out.println("\n\n");
		romulaner.phaserkanonenSchießen(klingonen);
		System.out.println("\n\n");
		vulkanier.sendeBroadcast("Gewalt ist nicht logisch");
		System.out.println("\n\n");

//		DEBUGGING
//		System.out.println(vulkanier.getBroadcastKommunikator().get(vulkanier.getBroadcastKommunikator().size()-1));
//		System.out.println(romulaner.getBroadcastKommunikator().get(romulaner.getBroadcastKommunikator().size()-1));

		// Nachricht ausgeben (anzeigen des BroadcastKommunikators)

		klingonen.statusEinsehen();
		klingonen.getLadungsverzeichnis();

		System.out.println("\n\n");
		vulkanier.sendeReparaturauftrag(true, true, true, vulkanier.getAnzahlReparaturAndroiden());
		System.out.println("\n\n");

		vulkanier.torpedosLaden(3); // Die Vulkanier haben oben (Zeile 21) drei Photonentorpedos zugewiesen
									// bekommen.
		vulkanier.ladungsverzeichnisAufraeumen();

		System.out.println("\n\n");

		klingonen.torpedosSchießen(romulaner);
		klingonen.torpedosSchießen(romulaner);

		System.out.println("\n\nKlingonen:");
		klingonen.statusEinsehen();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("\n\nRomulaner:");
		romulaner.statusEinsehen();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("\n\nVulkanier:");
		vulkanier.statusEinsehen();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("\n\n");

		System.out.println("\nBroadcastKommunikator:");
		for (String string : Raumschiff.erhalteLogbucheintraege()) {
			System.out.println(string);
		}

	}

}


