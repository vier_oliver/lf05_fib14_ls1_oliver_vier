import java.util.ArrayList;

public class HighscoreTable {

	// Die Implementation erfolgt hier leicht anders als wie in der
	// Aufgabenstellung.
	// Eintr�ge werden zun�chst normal hinzugef�gt, sprich am Ende der Liste.
	// Mithilfe der sort() Methode werden die Eintr�ge dann mit dem
	// Bubblsort-Algorithmus nach dem
	// Highscore sortiert.

	/*
	 * Hinzuf�gen -> addEntry() Ausgabe der gesamten Liste -> printList() L�schen
	 * von Eintr�gen -> removeEntry()
	 * Sortieren der Eintr�ge -> sort()
	 */

	private ArrayList<HighscoreEntry> entries = new ArrayList<HighscoreEntry>();

	public HighscoreTable() {

	}

	public HighscoreTable(ArrayList<HighscoreEntry> entries) {

		// Check for duplicate entries
		ArrayList<HighscoreEntry> toBeRemovedEntries = new ArrayList<HighscoreEntry>();
		for (int i = 0; i < entries.size(); i++) {
			for (int z = i; z < entries.size(); z++) {
				if (!(entries.get(i).equals(entries.get(z)))
						&& entries.get(i).getName().equals(entries.get(z).getName())) {
					if (entries.get(i).getHighscore() == entries.get(z).getHighscore()) {
						toBeRemovedEntries.add(entries.get(z));
					}
				}
			}
		}

		// Delete entries
		for (int i = 0; i < toBeRemovedEntries.size(); i++) {
			entries.remove(toBeRemovedEntries.get(i));
		}

		this.entries = entries;
	}

	public ArrayList<HighscoreEntry> getEntries() {
		return entries;
	}

	public void setEntries(ArrayList<HighscoreEntry> entries) {
		this.entries = entries;
	}

	public void addEntry(String name, int highscore) {

		if (checkDuplicateEntry(name, highscore)) {
			return;
		}
		entries.add(new HighscoreEntry(name, highscore));

	}

	public void removeEntry(int index) {
		entries.remove(index);
	}

	public void removeEntry(String name, int highscore) {
		for (HighscoreEntry entry : entries) {
			if (entry.getName().equals(name)) {
				if (entry.getHighscore() == highscore) {
					entries.remove(entry);
					return;
				}
			}
		}
	}

	public void sort() {
		int n = entries.size();
		boolean tauschbar;
		do {
			tauschbar = false;
			for (int i = 0; i < n - 1; ++i) {
				if (entries.get(i).getHighscore() > entries.get(i + 1).getHighscore()) {

					HighscoreEntry local = new HighscoreEntry(entries.get(i + 1).getName(),
							entries.get(i + 1).getHighscore());
					entries.get(i + 1).setName(entries.get(i).getName());
					entries.get(i + 1).setHighscore(entries.get(i).getHighscore());

					entries.get(i).setName(local.getName());
					entries.get(i).setHighscore(local.getHighscore());

					local = null;
					tauschbar = true;
				}
			}
			n = n - 1;
		} while (tauschbar);
	}

	public void printList() {
		int index = 0;
		for (HighscoreEntry entry : entries) {
			System.out.println(index + " - " + entry.getName() + " - " + entry.getHighscore());
			index++;
		}
	}

	public boolean checkDuplicateEntry(String name, int highscore) {
		// Check, if an entry with the values already exists
		for (HighscoreEntry entry : entries) {
			if (entry.getName().equals(name)) {
				if (entry.getHighscore() == highscore) {
					System.out.println("Entry already exists");
					return true;
				}
			}
		}
		return false;
	}
}
