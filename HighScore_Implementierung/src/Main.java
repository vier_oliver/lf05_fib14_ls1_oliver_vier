import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		
		ArrayList<HighscoreEntry> entries = new ArrayList<HighscoreEntry>();

		entries.add(new HighscoreEntry("Name0",20));
		entries.add(new HighscoreEntry("Name1",60));
		entries.add(new HighscoreEntry("Name2",30));
		entries.add(new HighscoreEntry("Name3",10));

		
		
		HighscoreTable table = new HighscoreTable(entries);
		
		table.sort();

		table.printList();

	}

}
