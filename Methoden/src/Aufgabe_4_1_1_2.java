import java.util.Scanner;
public class Aufgabe_4_1_1_2 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int zahl1, zahl2;
		
		System.out.println("Zahl 1: ");
		zahl1 = scan.nextInt();
		
		System.out.println("Zahl 2: ");
		zahl2 = scan.nextInt();
		
		if(zahl1 == zahl2) {
			System.out.println("Meldung. Die Zahlen sind gleich.");
		}
	}

}
