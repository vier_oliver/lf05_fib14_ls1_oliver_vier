import java.util.Scanner;

public class Aufgabe_3_2_3_2 {
	Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel;
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		
		// Benutzereingaben lesen
		artikel = liesString("Was m�chten Sie bestellen?"); //Artikel
		anzahl = liesInt("Geben Sie die Anzahl ein:"); //Anzahl
		preis = liesDouble("Geben Sie den Nettopreis ein:"); //Preis
		mwst = liesMwst("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		// Verarbeiten

		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		// Ausgeben
		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);


	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	//Es gab keine separate Methode f�r die Mwst.
	public static double liesMwst(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double mwst = myScanner.nextDouble();
		return mwst;
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,	double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}


}
