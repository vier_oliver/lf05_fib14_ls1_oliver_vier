//Anmerkung: Es gibt keine Aufgabe 3.

/**
 *a) Würfel: V = a * a * a
 *b) Quader: V = a * b * c
 *c) Pyramide: V = a * a * h / 3
 *d) Kugel: V = 4/3 * r^� * PI.  
 */
public class Aufgabe_3_2_1_4 {

	public static void main(String[] args) {
		
		double volumen;
		
		volumen = berechneWuerfel(2);
		System.out.println("Volumen eines W�rfels: " + volumen);
		volumen = berechneQuader(2,2,2);
		System.out.println("Volumen eines Quaders: " + volumen);
		volumen = berechnePyramide(5,6);
		System.out.println("Volumen einer Pyramide: " + volumen);
		volumen = berechneKugel(6);
		System.out.println("Volumen einer Kugel: " + volumen);

	}
	
	public static double berechneWuerfel(double a) {
		return (Math.pow(a, 3));
	}
	
	public static double berechneQuader(double a, double b, double c) {
		return (a*b*c);
	}

	public static double berechnePyramide(double a, double h) {
		return (a*a*(h/3.0));
	}

	public static double berechneKugel(double r) {
		return (4.0/3.0) * Math.PI * Math.pow(r, 3);
	}

}
