import java.util.Scanner;
public class Aufgabe_4_1_5_BMI {

	//Aufgabe 7 Sortieren befindet sich im Ordner "Konfigurationschaos"!
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		double gewicht, groesse, bmiWert;
		char geschlecht;
		
		System.out.println("Gebe dein Gewicht in kg an: ");
		gewicht = scan.nextDouble();
		System.out.println("Gebe deine Groe�e in Meter an: ");
		groesse = scan.nextDouble();
		System.out.println("Gebe dein Geschlecht an (m/w): ");
		geschlecht = scan.next().charAt(0);
		
		
		//Berechnung des BMI-Wertes
		
		bmiWert = gewicht / Math.pow(groesse, 2);
		System.out.println(bmiWert);
		
		if(geschlecht == 'm' && bmiWert > 25) {
			System.out.println("(M) Du bist zu schwer.");
		} else if(geschlecht == 'm' && bmiWert <= 25 && bmiWert >= 20) {
			System.out.println("(M) Du bist im gr�nen Bereich.");
		} else if(geschlecht == 'm' && bmiWert < 20){
			System.out.println("(M) Du bist zu leicht.");
		}
		
		if(geschlecht == 'w' && bmiWert > 24) {
			System.out.println("(W) Du bist zu schwer.");
		} else if(geschlecht == 'w' && bmiWert <= 24 && bmiWert >= 19) {
			System.out.println("(W) Du bist im gr�nen Bereich.");
		} else if(geschlecht == 'w' && bmiWert < 19){
			System.out.println("(W) Du bist zu leicht.");
		}
		
		
		
	}

}
