import java.util.Scanner;
public class Aufgabe_4_1_3_HardwareGrosshaendler {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		int anzahlMaeuse;
		double nettopreis, bruttopreis;

		System.out.println("Wie viele M�use willst du kaufen? ");
		anzahlMaeuse = scan.nextInt();
		System.out.println("Wie viel kosten die M�use? ");
		nettopreis = scan.nextDouble();
		
		if(anzahlMaeuse >= 10) {
			bruttopreis = (nettopreis * anzahlMaeuse) * 1.19;
			System.out.printf("Der Bruttopreis betr�gt %.2f Euro\nNettopreis: %.2f Euro\n+ Mehrwertsteuer: %.2f Euro", bruttopreis, nettopreis * anzahlMaeuse, (nettopreis * anzahlMaeuse)*0.19);
		} else if(anzahlMaeuse <= 0) {
			System.out.println("Du kannst nicht 0 oder weniger M�use kaufen.");
		} else {
			bruttopreis = ((nettopreis * anzahlMaeuse) * 1.19) + 10; //Lieferpauschale
			System.out.printf("Der Bruttopreis betr�gt %.2f Euro\nNettopreis: %.2f Euro\n+ Mehrwertsteuer: %.2f Euro", bruttopreis, nettopreis * anzahlMaeuse, (nettopreis * anzahlMaeuse)*0.19);
		}

	}

}
