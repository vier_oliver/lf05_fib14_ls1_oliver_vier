import java.util.Scanner;
public class Aufgabe_4_1_2_Steuersatz {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Gib einen Nettowert ein: ");
		double nettowert = scan.nextDouble();
		double bruttowert;
		char caseWert = 'd'; //Wert d als "default"
		
		while(caseWert != 'j' && caseWert != 'n') {
			
			System.out.println("M�chtest du den erm��igten Steuersatz nehmen? (j -> 7%, n -> 19%)");
			caseWert = scan.next().charAt(0);
			
			if(caseWert == 'j') {
				bruttowert = nettowert * 1.07;
				System.out.printf("%.2f",bruttowert);
				break;
			} else if(caseWert == 'n') {
				bruttowert = nettowert * 1.19;
				System.out.printf("%.2f",bruttowert);
				break;
			}
		}
		

	}

}
