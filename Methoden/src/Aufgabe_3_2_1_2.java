
public class Aufgabe_3_2_1_2 {

	public static void main(String[] args) {
		
		double wert1 = 2.36;
		double wert2 = 7.87;
		double ergebnis = multiplizieren(wert1, wert2);
		System.out.println(ergebnis);
		
	}
	
	public static double multiplizieren(double wert1, double wert2) {
		return (wert1*wert2);
	}

}
