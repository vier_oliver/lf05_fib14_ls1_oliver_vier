import java.util.Scanner;
public class Aufgabe_4_1_6_Funktionsloeser {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Gib deinen X-Wert an: ");
		float xWert = scan.nextFloat();
		float berechnungsWert;
		
		if(xWert <= 0) {
			//Exponentiell
			float E_WERT = 2.718f;
			berechnungsWert = (float) Math.pow(E_WERT, xWert);
			System.out.println("Exponentiell: Die Berechnung ergibt " + berechnungsWert);
		} else if(xWert > 0 && xWert <= 3) {
			berechnungsWert = (float) Math.pow(xWert, 2) + 1;
			System.out.println("Quadratisch: Die Berechnung ergibt " + berechnungsWert);
		} else if(xWert > 3) {
			berechnungsWert = (float) 2 * xWert + 4;
			System.out.println("Linear: Die Berechnung ergibt " + berechnungsWert);
		}

	}

}
