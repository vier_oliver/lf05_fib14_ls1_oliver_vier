import java.util.*;
public class Aufgabe_3_2_1_5 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		//Ich habe das Verwenden von Objekten erstmal in dieser Aufgabe ausgelassen.
		
		String l�nderNamen[] = {"United States of America","Japan","England","Schweiz","Schweden"};
		String l�nderAbkuerzungen[] = {"USA","JPN","ENG","CHE","SWE"};
		String l�nderWaehrungen[] = {"Dollar","Yen","Pfund","Schweizer Franken","Schwedische Kronen"};
		double wechselKurse[] = {1.22, 126.50, 0.89, 1.08, 10.10};
		
		//Zusatzaufgabe a) Reisebudget
		System.out.println("Gib deine Reisebudget an");
		double reiseBudget = scan.nextDouble();
		//Zusatzaufgabe b) Reservebudget
		System.out.println("Gib deinen Reserve-Betrag an");
		double reserveBetrage = scan.nextDouble();
		
		
		char userInput = 'J';
		String landAuswahl;
		double wertBetrag;
		
		int inkr = -1;
		
		while(userInput != 'N' && userInput != 'n') {
			
			System.out.println("W�hle das Gastland aus: ");
			for (int i = 0; i < l�nderNamen.length; i++) {
				System.out.println(l�nderNamen[i] + " -> " + l�nderAbkuerzungen[i]);			
			}
			
			landAuswahl = scan.next();
			//�berpr�fen, ob die Abk�rzung vorhanden ist.
			for(int i = 0; i < l�nderNamen.length; i++) {
				if(landAuswahl.toUpperCase().equals(l�nderAbkuerzungen[i].toUpperCase())) {
					inkr = i;
					break;
				}
			} 
				System.out.println("Wie viel Geld soll umgetauscht werden? ");
				wertBetrag = scan.nextDouble();
				if(reiseBudget-wertBetrag < 0) {
					System.out.println("Du kannst nicht mehr Geld umtauschen als du hast.");
					while(reiseBudget-wertBetrag < 0) {
						System.out.println("Gebe einen neuen Wert zum Umtauschen ein.");
						wertBetrag = scan.nextDouble();
					}
				}
				reiseBudget -= wertBetrag;
				
				System.out.printf("F�r das Land " + l�nderNamen[inkr] + " bekommst du f�r %.2f"
						+ " Euro so viel Geld in der W�hrung: %.2f "+l�nderWaehrungen[inkr]+"\n", wertBetrag, (wertBetrag*wechselKurse[inkr]));
			
			if(reiseBudget <= reserveBetrage) {
				System.out.println("Huch, du hast deinen Reservebetrag unterschritten. Ab nach Hause.");
				break;
			}

			
			inkr = -1;
			System.out.println("Neuer Durchlauf? Nein -> N/n");
			userInput =  scan.next().charAt(0);
		}

	}

}
