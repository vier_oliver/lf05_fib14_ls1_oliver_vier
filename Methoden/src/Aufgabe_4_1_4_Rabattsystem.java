import java.util.Scanner;
public class Aufgabe_4_1_4_Rabattsystem {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Gebe deinen Bestellwert an: ");
		double bestellwert = scan.nextInt();
		double bruttowert = 0;
		double rabatt;
		
		if(bestellwert <= 100 && bestellwert >= 0) {
			
			rabatt = 0.1;
			bruttowert = (bestellwert * (1-rabatt)) * 1.19 ;
			System.out.printf("Bruttowert: %.2f Euro\n+ Bestellwert: %.2f Euro\n+ Rabatt: %.2f Euro\n+ MWST 19 Prozent: %.2f Euro", bruttowert, bestellwert, (bestellwert * rabatt), (bestellwert * rabatt) * 0.19);
		} else if(bestellwert <= 500 && bestellwert > 100) {
			
			rabatt = 0.15;
			bruttowert = (bestellwert * (1-rabatt)) * 1.19 ;
			System.out.printf("Bruttowert: %.2f Euro\n+ Bestellwert: %.2f Euro\n+ Rabatt: %.2f Euro\n+ MWST 19 Prozent: %.2f Euro", bruttowert, bestellwert, (bestellwert * rabatt), (bestellwert * rabatt) * 0.19);
				
		} else if(bestellwert > 500) {
			
			rabatt = 0.2;
			bruttowert = (bestellwert * (1-rabatt)) * 1.19 ;
			System.out.printf("Bruttowert: %.2f Euro\n+ Bestellwert: %.2f Euro\n+ Rabatt: %.2f Euro\n+ MWST 19 Prozent: %.2f Euro", bruttowert, bestellwert, (bestellwert * rabatt), (bestellwert * rabatt) * 0.19);
		}	

	}

}
