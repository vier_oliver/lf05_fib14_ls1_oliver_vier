package Blatt_1;

public class Lotto {

	public static void main(String[] args) {
		
		int lottozahlen[] = {3,7,12,18,37,42};
		
		System.out.print("[ "); 
		for(int i : lottozahlen) { System.out.print(i + " "); }
		System.out.print("]");
		
		int zahl;
		
		zahl = 12;
		System.out.println(containsNumber(lottozahlen, zahl) ? "\n\nDie Zahl " + zahl + " ist in der Ziehung enthalten." : "\n\nDie Zahl " + zahl + " ist nicht in der Ziehung enthalten.");
		
		zahl = 13;
		System.out.println(containsNumber(lottozahlen, zahl) ? "Die Zahl " + zahl + " ist in der Ziehung enthalten." : "Die Zahl " + zahl + " ist nicht in der Ziehung enthalten.");
		
	}
	
	public static boolean containsNumber(int liste[], int gesuchteZahl) {
		
		for (int i : liste) {
			if(i == gesuchteZahl) {
			
				return true;
			
			}	
		}
		return false;
	}

}
