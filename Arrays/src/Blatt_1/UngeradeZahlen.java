package Blatt_1;

public class UngeradeZahlen {

	public static void main(String[] args) {

		int werteListe[] = new int[10];
		int inkr = 0;
		
		for(int i = 1; i <= 19; i++) {
			if(i%2==1) {
				werteListe[inkr] = i;
				inkr++;
			}
		}

		for (int i : werteListe) {
			System.out.print(i + " ");
		}

	}

}
