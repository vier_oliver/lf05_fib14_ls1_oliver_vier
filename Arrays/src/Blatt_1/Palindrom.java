package Blatt_1;
import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char zeichenListe[] = new char[5];
		
		for(int i = 0; i < zeichenListe.length; i++) {
			System.out.print(i+1 + ". Gebe hier dein Zeichen ein: ");		//Bei der Eingabe k�nnte man auch einfach einen String mit 5 Zeichen einlesen,
			zeichenListe[i] = scan.next().charAt(0);						//bei welchen jedes Zeichen (dann als Char) getestet wird.
		}
		
		System.out.println("\nUmgekehrte Ausgabe: ");
		
		for(int i = zeichenListe.length-1; i>=0; i--) {
			System.out.print(zeichenListe[i] + " ");
		}
		
		//Zusatz: �berpr�fen, ob es Palindrom ist.

		System.out.println(isPalindrom(zeichenListe) ? "\nEs ist ein Palindrom." : "\nEs ist kein Palindrom");
	}	
	
	public static boolean isPalindrom(char zeichenListe[]) {
		
		for(int i = 0; i < zeichenListe.length/2; i++) {
			if(zeichenListe[i] != zeichenListe[(zeichenListe.length-1)-i]) {
				return false;
			}
		}
		return true;
	}
}
