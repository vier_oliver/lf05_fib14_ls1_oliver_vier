package Blatt_2;
import java.util.Scanner;

public class Aufgabe_5_2_2_2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		String values = "";
		
		try {
			
			System.out.println("Gebe die L�nge des Arrays an: ");
			int werteTabelle[] = new int[scan.nextInt()];
			
			System.out.println("Gib den gr��ten Wert an (Werte: x>0): ");
			int sizeLimit = scan.nextInt();
			
			for(int idx = 0; idx < werteTabelle.length; idx++) {
				werteTabelle[idx] = (int) (Math.random()*sizeLimit);
				values += String.valueOf(werteTabelle[idx]) + " ";
			}
			
			System.out.printf("%s%"+ (values.length()+4) + "s\n", "Deine Werte lauten: ", values);
			System.out.printf("%s%s\n", "Umgekehrt lauten diese: ", reverseOrder(werteTabelle));
			
		} catch (Exception e) {
			
			System.out.println("Starte das Programm neu" + e);
			
		}
		
		scan.close();

	}
	
	public static String reverseOrder(int werteTabelle[]) {
		
		String values = "";
		
		int arrayLimit = werteTabelle.length-1;
		int saveValue;
		
		for(int idx = 0; idx < werteTabelle.length/2; idx++) {

			saveValue = werteTabelle[idx];
			werteTabelle[idx] = werteTabelle[arrayLimit-idx];
			werteTabelle[arrayLimit-idx] = saveValue;
			
		}
		
		for(int i = 0; i < werteTabelle.length; i++) {
			values += werteTabelle[i] + " "; 
		}
		
		return values;
		
	}
}

