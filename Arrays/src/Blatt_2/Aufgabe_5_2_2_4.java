package Blatt_2;
import java.util.Scanner;

public class Aufgabe_5_2_2_4 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);

		int zeilenAnzahl;
		
		System.out.println("Gebe die Anzahl an Zeilen als Ganzzahl an: ");
		zeilenAnzahl = scan.nextInt();
		
		float werteTabelle[][] = berechneCelsius(zeilenAnzahl);
		int maxStringSize = String.valueOf(werteTabelle[werteTabelle.length-1][0]).length()+1;
		for(int i = 0; i < werteTabelle.length; i++) {
			System.out.printf("%-"+ maxStringSize +".2f F %s %.2f C\n",werteTabelle[i][0],"<=>" ,werteTabelle[i][1]);
		}
	}
	
	public static float[][] berechneCelsius(int zeilenAnzahl) {
		float werteTabelle[][] = new float[zeilenAnzahl][2];
		
		for(int i = 0; i < werteTabelle.length; i++) {
			werteTabelle[i][0] = i*10;
			werteTabelle[i][1] = (5.0f/9.0f)*(werteTabelle[i][0]-32);
		}
		
		return werteTabelle;
	}

}
