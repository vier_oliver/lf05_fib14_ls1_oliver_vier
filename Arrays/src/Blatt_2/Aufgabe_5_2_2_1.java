package Blatt_2;

public class Aufgabe_5_2_2_1 {

	public static void main(String[] args) {
		
		int zahlen[] = {2,3,6,8,3}; //Test-Array
		System.out.println(convertArrayToString(zahlen));
	
	}
	
	public static String convertArrayToString(int[] zahlen) {
		
		String localArray = "";
		
		for(int i = 0; i < zahlen.length; i++) {
			
			localArray += (i != zahlen.length-1) ? String.valueOf(zahlen[i]) + ", " : String.valueOf(zahlen[i]);
		
		}
		
		return localArray;
	}

}
