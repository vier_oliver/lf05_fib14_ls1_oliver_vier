package Blatt_2;
import java.util.Scanner;

/*
 * 
 * Da ich die Aufgabe anders verstanden hatte, habe ich diese Version erstellt.
 * Die vermutlich richtige L�sung befindet sich bei "Aufgabe_5_2_2_5_2"
 * Die n�chste Aufgabe basiert auf der anderen L�sung.
 * 
 */

public class Aufgabe_5_2_2_5_2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Eingabe von m, darauffolgend von n: ");
		int m = scan.nextInt(), n = scan.nextInt();
		
		int matrixListe[][] = erstelleMatrix(m, n);
		
		int maxStringLaenge = String.valueOf(matrixListe[m-1][n-1]).length();
		
		for(int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.printf("%-" + (maxStringLaenge+1) + "d",matrixListe[i][j]); //maxStringLaenge => +1 f�r eine L�cke
			}
			System.out.println("");
		}
		
		

	}
	
	public static int[][] erstelleMatrix(int m, int n){
		
		Scanner scan = new Scanner(System.in);
		int matrixListe[][] = new int[m][n];
		
		for(int i = 0; i < m; i++) {
			for(int k = 0; k < n; k++) {
				System.out.println("Spalte "+(i+1) + " Zeile " +(k+1));
				matrixListe[i][k] = scan.nextInt();
			}
		}
		
		
		
		
		return matrixListe;
	}

}
