package Blatt_2;
import java.util.Scanner;

public class Aufgabe_5_2_2_6 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Eingabe von m, darauffolgend von n: ");
		int m = scan.nextInt(), n = scan.nextInt();
		
		int matrixListe[][] = erstelleMatrix(m, n);
		int quersummeBasis[] = new int[matrixListe.length];

		for(int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.printf("%-4d",matrixListe[i][j]); //maxStringLaenge => +1 f�r eine L�cke
				quersummeBasis[i] += matrixListe[i][j];
			}
			System.out.print(" Q: "+quersummeBasis[i]+"\n");
			System.out.println("");
		}
		
		System.out.println("");
		
		int transMatrixListe[][] = transponiereMatrix(matrixListe);
		int quersummeTrans[] = new int[transMatrixListe.length];

		for(int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.printf("%-4d",transMatrixListe[i][j]); //maxStringLaenge => +1 f�r eine L�cke
				quersummeTrans[i] += transMatrixListe[i][j];
			}
			System.out.print(" Q: "+quersummeTrans[i]+"\n");
			System.out.println("");
		}
		
		System.out.println(vergleicheMatrizen(quersummeBasis, quersummeTrans) ? "Selbst-transponiert - trifft zu." : "Selbst-transponiert - trifft nicht zu.");
			
		scan.close();
	}
	
	public static int[][] erstelleMatrix(int m, int n){
		
		Scanner scan = new Scanner(System.in);
		int matrixListe[][] = new int[m][n];
		
		for(int i = 0; i < m; i++) {
			for(int k = 0; k < n; k++) {
				System.out.println("Spalte "+(i+1) + " Zeile " +(k+1));
				matrixListe[i][k] = scan.nextInt();
			}
		}
		
		scan.close();
		
		return matrixListe;
	}
	
	public static int[][] transponiereMatrix(int matrix[][]){
		
		int transMatrix[][] = new int[matrix[0].length][matrix.length]; 
				
		for(int i = 0; i < matrix.length; i++ ) {
			for(int k = 0; k < matrix[0].length; k++) {
				transMatrix[k][i] = matrix[i][k];
			}
		}

		return transMatrix;
	}
	
	public static boolean vergleicheMatrizen(int quersummeN[], int quersummeT[]) {

		//Wenn eine unterschiedliche Quersumme gefunden wird, kann man sofort von einen falschen Ergebnis ausgehen.
		if(quersummeN.length != quersummeT.length) {
			System.out.println(quersummeN.length + "  " + quersummeT.length);
			return false;
		}
		
		//Jede Quersumme wird �berpr�ft (Normale Matrix mit Transponierter).
		for(int i = 0; i < quersummeN.length; i++) {
			System.out.println("Vergleich: Q: " + quersummeN[i] + " Q: " + quersummeT[i]);
			if(quersummeN[i] != quersummeT[i]) {
				return false;
			}
		}
		
		return true;
	}

}
