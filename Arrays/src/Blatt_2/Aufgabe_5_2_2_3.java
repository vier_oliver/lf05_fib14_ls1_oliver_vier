package Blatt_2;
import java.util.Scanner;

public class Aufgabe_5_2_2_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		String values = "";
		
		try {
			
			System.out.println("Gebe die L�nge des Arrays an: ");
			int werteTabelle[] = new int[scan.nextInt()];
			
			System.out.println("Gib den gr��ten Wert an (Werte: x>0): ");
			int sizeLimit = scan.nextInt();
			
			for(int idx = 0; idx < werteTabelle.length; idx++) {
				werteTabelle[idx] = (int) (Math.random()*sizeLimit);
				values += String.valueOf(werteTabelle[idx]) + " ";
			}
			
			System.out.printf("%s%"+ (values.length()+4) + "s\n", "Deine Werte lauten: ", values);
			
			System.out.print("Umgekehrt lauten diese: ");
			
			int werteTabelleReversed[] = reverseOrder(werteTabelle);
			for (int i : werteTabelleReversed) {
				System.out.print(i + " ");
			}
			
			
		} catch (Exception e) {
			
			System.out.println("Starte das Programm neu" + e);
			
		}
	}
	
	public static int[] reverseOrder(int[] liste) {
		
		int werteTabelle[] = new int[liste.length];
		
		for(int i = 0; i < liste.length; i++) {
			werteTabelle[i] = liste[(liste.length-1)-i];
		}
		
		return werteTabelle;
	}

}
