package whileSchleifen;
import java.util.Scanner;

public class Temperaturumrechnung {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Bitte den Startwert in Celsius angeben: ");
		float startValue = scan.nextFloat();
		System.out.print("Bitte den Endwert in Celsius angeben: ");
		float limiterValue = scan.nextFloat();
		System.out.print("Bitte die Schrittweite in Celsius angeben: ");
		float stepValue = scan.nextFloat();
		System.out.print("\n");
		
		while(startValue <= limiterValue) {
			
			System.out.printf("%5.2f�C %5.2f�F\n", startValue, startValue*9/5+32);
			startValue += stepValue;
			
		}

	}

}
