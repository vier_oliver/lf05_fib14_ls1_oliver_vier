package whileSchleifen;
import java.util.Scanner;
public class Matrix {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Gebe bitte eine Zahl von 2 bis 9 ein: ");
		int matrixZahl = scan.nextInt();

		int y = 1;
		while(y <= 100) {
		
			if(containsNumber(y, matrixZahl) || isQuersumme(y, matrixZahl) || y%matrixZahl==0) {
				System.out.printf("%-3s", "*");	
			}
			else {
				System.out.printf("%-3s" , y);	
			}
			
			if(y%10==0) {System.out.println("");}
			y++;
		}
	}
	
	public static boolean containsNumber(int value, int matrixZahl) {
		
		String strValue = String.valueOf(value);
		
		//An Stelle idx wird �berpr�ft, ob die Matrixzahl vorhanden ist. Ist das der Fall, dann wird "true" zur�ckgegeben.
		int idx = 0;
		while(idx < strValue.length()) {
			if(strValue.charAt(idx) == String.valueOf(matrixZahl).charAt(0)) {
				return true;
			}
			idx++;
		}
		return false;
	}
	
	//�berpr�ft anhand des aktuellen Wertes und der angegebenen Matrix-Zahl, ob eine Quersumme vorhanden ist.
	public static boolean isQuersumme(int value, int matrixZahl) {
		
		String strValue = String.valueOf(value);
		int valueList[] = new int[3];
		
		//Aufteilen in ein String und dann dessen Char-Extraktion.
		int i = 0;
		while(i < strValue.length()) {
			valueList[i] = Integer.parseInt(String.valueOf(strValue.charAt(i)));
			i++;
		}
		
		//Die Quersumme wird aus den einzelnen Ziffern berechnet -> 270 -> 2 + 7 + 0 = 9
		int quersumme = valueList[0] + valueList[1] + valueList[2]; 
		
		//System.out.println(quersumme);
		
		//Entspricht die Quersumme der Matrixzahl, so wird true zur�ckgegben. Wird nichts gefunden, so false.
		if(quersumme == matrixZahl) {
			return true;
		} else {
			return false;	
		}
		
	}

}
