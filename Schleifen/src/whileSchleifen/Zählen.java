package whileSchleifen;
import java.util.Scanner;

public class Z�hlen {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Gebe bitte den Wert n an: ");
		int n = scan.nextInt();
		
		System.out.print("a) ");
		werteAusgeben(true, n);
		System.out.print("b) ");
		werteAusgeben(false, n);
		
		
		scan.close();

	}
	
	public static void werteAusgeben(boolean aufsteigend, int n) {
		
		if(aufsteigend == true) {
			int i = 1;
			while(i <= n) {
				if(i < n) {
					System.out.print(i+", ");
				} else{
					System.out.print(i);
				}
				i++;
			}
		}

		if(aufsteigend == false) {
			int i = n;
			while(i > 0) {
				if(i > 1) {
					System.out.print(i+", ");
				} else{
					System.out.print(i);
				}
				
				i--;
			}
		}

		System.out.println("");
	}

}
