package whileSchleifen;
import java.util.Scanner;
public class Million {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		double anzahlJahre = 0;
		System.out.println("-x- Der Million-Rechner -x- ");
		System.out.println("Wie viel investierst du in Euro? ");
		double einlage = scan.nextDouble();
		System.out.println("Wie hoch ist der Zinssatz? ");
		double zinssatz = scan.nextDouble(); 
		double zwischenwert = 0;

		while(zwischenwert <= 1000000) {
			anzahlJahre++;
			zwischenwert = einlage*Math.pow(1+(zinssatz/100),anzahlJahre);
			System.out.printf("%.0f Jahre: %.2f �\n", anzahlJahre, zwischenwert);
		}
		
		
		System.out.printf("Antwortsatz: Es dauert %.0f Jahre, bis die eine Million erreicht werden.", anzahlJahre);
		
	}

}
