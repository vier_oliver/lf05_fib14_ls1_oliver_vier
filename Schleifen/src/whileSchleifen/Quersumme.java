package whileSchleifen;
import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		String summe = scan.next();
		
		char summenCharList[] = addCharacters(summe);
		int quersumme = berechneQuersumme(summenCharList);
		
		System.out.println("Die Quersumme betr�gt: " + quersumme);
		scan.close();
		
	}
	
	public static char[] addCharacters(String summe) {
		
		char summenCharList[] = new char[summe.length()];
		
		int i = 0;
		while(i<summenCharList.length) {
			summenCharList[i] = summe.charAt(i);
			i++;
		}
		
		return summenCharList;
		
	}
	
	public static int berechneQuersumme(char[] summenCharList) {
		
		int limiter = summenCharList.length;
		int summe = 0;
		
		int i = 0;
		while(i < limiter) {
			summe += Integer.parseInt(String.valueOf(summenCharList[i]));
			i++;
		}
		
		return summe;
		
	}

}
