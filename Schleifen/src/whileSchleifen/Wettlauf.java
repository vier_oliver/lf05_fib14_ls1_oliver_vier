package whileSchleifen;

public class Wettlauf {

	public static void main(String[] args) {
		
		//Distanz in Meter
		double distanz = 1000;
		
		//Gibt an, wie weit die L�ufer bereits gelaufen sind.
		//SP stehend f�r Startpunkt.
		double laeufer1DistSP = 0;
		double laeufer2DistSP = 250;
		
		//Gibt an, wie schnell die L�ufer sind (in m/s).
		double laeuefer1Speed = 9.5f;
		double laeuefer2Speed = 7;
		
		int zeitReferenz = 0;
		System.out.printf("Start: %ds: L�ufer 1: %.2fm  L�ufer 2: %.2fm \n", zeitReferenz, laeufer1DistSP, laeufer2DistSP);
		
		do {
			laeufer1DistSP += laeuefer1Speed;
			laeufer2DistSP += laeuefer2Speed;
			zeitReferenz++;
			System.out.printf("%ds: L�ufer 1: %.2fm  |  L�ufer 2: %.2fm \n", zeitReferenz, laeufer1DistSP, laeufer2DistSP);
		} while (laeufer1DistSP <= distanz && laeufer2DistSP <= distanz);

		//Genaue L�sung ohne Schleife
		// v=s/t => t = s/v
		double laeufer1Erg = (1000)/laeuefer1Speed;
		double laeufer2Erg = (1000-250)/laeuefer2Speed;
		
		System.out.printf("\nDer erste L�ufer erreicht das Ziel in %.2f Sekunden.\n", laeufer1Erg);
		System.out.printf("Der zweite L�ufer erreicht das Ziel in %.2f Sekunden.\n", laeufer2Erg);
		
		
	}

}
