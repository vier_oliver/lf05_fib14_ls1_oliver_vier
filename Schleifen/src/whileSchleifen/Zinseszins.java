package whileSchleifen;
import java.util.Scanner;

public class Zinseszins {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Laufzeit (in Jahren) des Sparvertrages: ");
		int laufzeitJahre = scan.nextInt();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen? ");
		float kapital = scan.nextFloat();
		System.out.print("Zinssatz: ");
		float zinssatz = scan.nextFloat();
		
		float ausgezahlterWert = kapital;
		
		int i = 0;
		while(i < laufzeitJahre) {
			//ausgezahlterWert += (float)(kapital*Math.pow(1+(zinssatz/100),1));
			ausgezahlterWert += ausgezahlterWert*(zinssatz/100);
			i++;
		}
		
		System.out.printf("\nEingezahlter Betrag: %.2f Euro\n", kapital);
		System.out.printf("Ausgezahlter Betrag: %.2f Euro\n", ausgezahlterWert);
		

	}

}
