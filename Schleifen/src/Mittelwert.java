

//Quelle: LF5 Kurs "Mittelwert.java" Zugriff: 08.12.2021
//Link: https://moodle.oszimt.de/mod/url/view.php?id=279006 
import java.util.ArrayList;
import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

	   Scanner scan = new Scanner(System.in);
	   ArrayList<Double> werteList = new ArrayList<Double>();
       double m = 0;
     	
	   while(true) {
		   
		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
	    // ===========================
	   
		
		System.out.println("Gebe einen neuen Wert ein: ");
		werteList.add(scan.nextDouble());
      
      	// (V) Verarbeitung
      	// Mittelwert von x und y berechnen: 
      	// ================================
		
		for(int i = 0; i < werteList.size(); i++) {
			m += werteList.get(i);
		}
      	m = m / werteList.size();

      	// (A) Ausgabe
      	// Ergebnis auf der Konsole ausgeben:
      	// =================================
      	
      	System.out.print("Der Mittelwert von ");
      	for(int i = 0; i < werteList.size(); i++) {
      		if(i != werteList.size()-1) {
      			System.out.print(werteList.get(i)+", ");
      		} else {
      			System.out.print(werteList.get(i));
      		}
      	}
      	System.out.printf(" ist %.2f\n", m);
      	m=0;
	  }
   }
}
