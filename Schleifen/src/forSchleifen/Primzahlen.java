package forSchleifen;

public class Primzahlen {

	public static void main(String[] args) {
		
		boolean teilbarVar = false;
		int[] primzahlListe = new int[25];

		for(int i = 2; i < 100; i++) {
			for(int z = 2; z < (i / 2)+1; z++) {
				
				if(i%z == 0) {
					teilbarVar = true;
					break;
				}
				else if(i%z != 0) {
					teilbarVar = false;
				}
			}
			if(teilbarVar == false) {
				for(int s = 0; s < primzahlListe.length; s++) {
					if(primzahlListe[s] == 0 ) {
						primzahlListe[s] = i;
						break;
					}
				}
			}
		}
		
		System.out.print("P = {");
		for(int i = 0; i < primzahlListe.length; i++) {
			if(i != primzahlListe.length-1) {System.out.print(" "+primzahlListe[i]+",");}
			else {System.out.print(" "+primzahlListe[i]);}
		}
		System.out.print(" }");
	}

}
