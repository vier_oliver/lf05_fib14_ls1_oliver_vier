package forSchleifen;
import java.util.Scanner;
public class Treppe {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int hoehe = 0;
		do {
			System.out.println("H�he der Treppe (gr��er als 0): ");
			hoehe = scan.nextInt();
		} while (hoehe <= 0);
		
		int breite = 0;
		do {
			System.out.println("Breite der Treppe (gr��er als 0): ");
			breite = scan.nextInt();
		} while (breite <= 0);
		
		int freieStellen = 0;
		int belegteStellen = 0;
		for(int hIdx = 0; hIdx <= hoehe; hIdx++) {
			
			freieStellen = (hoehe-hIdx) * breite;
			for(int bIdx = 0; bIdx < freieStellen; bIdx++) {
				System.out.print(" ");
			}
			belegteStellen = (hoehe*breite)-freieStellen;
			for(int bIdx = 0; bIdx < belegteStellen; bIdx++) {
				System.out.print("*");
			}
			//Spacer
			System.out.println("");
		}

	}

}
