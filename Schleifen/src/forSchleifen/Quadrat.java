package forSchleifen;
import java.util.Scanner;
public class Quadrat {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Wie gro� soll dein Quadrat sein? ");
		int groesse = scan.nextInt();
		
		for(int y = 0; y < groesse; y++) {
			for(int x = 0; x < groesse; x++) {
				if(y == 0 || y == groesse-1) {
					System.out.print(" * ");
				}
				else { if(x == 0 || x == groesse-1) {
					System.out.print(" * ");
				} else {
					System.out.print("   ");
				}
				}
			}
			System.out.println();
		}
	}
}
