package forSchleifen;
import java.util.Scanner;

public class Z�hlen {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Gebe bitte den Wert n an: ");
		int n = scan.nextInt();
		
		System.out.print("a) ");
		werteAusgeben(true, n);
		System.out.print("b) ");
		werteAusgeben(false, n);
		
		
		scan.close();

	}
	
	public static void werteAusgeben(boolean aufsteigend, int n) {
		if(aufsteigend == true) {
			for(int i = 1; i <= n; i++) {
				if(i < n) {
					System.out.print(i+", ");
				} else{
					System.out.print(i);
				}
			}
		}
		
		if(aufsteigend == false) {
			for(int i = n; i > 0; i--) {
				if(i > 1) {
					System.out.print(i+", ");
				} else{
					System.out.print(i);
				}
			}
		}
		System.out.println("");
	}

}
