package forSchleifen;
import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Gebe den Limiter-Wert ein: ");
		int n = scan.nextInt();
		
		System.out.print("a) ");
		werteAusgeben(1, n);
		System.out.print("\nb) ");
		werteAusgeben(2, n);
		System.out.print("\nc) ");
		werteAusgeben(3, n);
		
		scan.close();

	}
	
	public static void werteAusgeben(int modus, int n){
		if(modus == 1) {
			for(int i = 1; i <= n; i++) {
					if(i != n) {
						System.out.print(i + ", ");
					} else {
						System.out.print(i);
					}
			}
		}
			
		if(modus == 2) {
			for(int i = 1; i <= n; i++) {
					if(i != n) {
						System.out.print(i*2 + ", ");
					} else {
						System.out.print(i*2);
					}
			}
		}
		
		if(modus == 3) {
			for(int i = 0; i < n; i++) {
				if(i+1 != n) {
					System.out.print(i*2+1 + ", ");
				} else {
					System.out.print(i*2+1);
				}
			}
		}
			
			
			
			
			
			/*
			if(modus == 2) {
				if(i+1 != n) {
					System.out.print(i*2 + ", ");
				} else {
					System.out.print(i*2);
				}
			}
			
			if(modus == 3) {
				if(i+1 != n) {
					System.out.print(i*2+1 + ", ");
				} else {
					System.out.print(i*2+1);
				}
			}	*/
			
		
	}
}
