package forSchleifen;

public class Modulo {

	public static void main(String[] args) {
		
		for(int i = 1; i <= 200; i++) {
			if(i%7 == 0) {
				System.out.println(i);
			} else if(i%5 != 0 && i%4==0) {
				System.out.println(i);
			}
		}
		
	}

}
