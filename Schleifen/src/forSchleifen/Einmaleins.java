package forSchleifen;

public class Einmaleins {

	public static void main(String[] args) {

		for(int zahl1 = 0; zahl1 <= 10; zahl1++) {
			System.out.print(zahl1 + "  ");
			for(int zahl2 = 1; zahl2 <= 10; zahl2++) {
				
				if(zahl1==0) {System.out.printf("%-3d" ,zahl2);}
				else {
					System.out.printf("%-2d ", zahl1*zahl2);
				}
			}
			System.out.println("");
		}

	}

}
