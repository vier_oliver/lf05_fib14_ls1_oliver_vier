package forSchleifen;

public class Folgen {
	public static void main(String[] args) {
		System.out.print("a) ");
		
		for(int i = 99; i >= 9; i = i-3) {
			System.out.print(i + " ");
		}
		
		System.out.print("\nb) ");
		
		int z = 3;
		for(int i = 1; i <= 400; z+=2) {
			System.out.print(i + " ");
			i+=z;
		}
		
		System.out.print("\nc) ");
		
		for(int i = 2; i <= 102; i+=4) {
			System.out.print(i + " ");
		}
		
		System.out.print("\nd) ");
		
		z = 12;
		for(int i = 4; i <= 1024; z+=8) {
			System.out.print(i + " ");
			i+=z;
		}
		
		System.out.print("\ne) ");
		
		for(int i = 2; i <= 32768; i*=2) {
			System.out.print(i + " ");
		}
	}
}
