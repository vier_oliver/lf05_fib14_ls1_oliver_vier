package IB_Aufgaben;

public class Aufgabe_5 {

	public static void main(String[] args) {

		float mittelwert = 0;
		float list[] = {1,2,3,4,5,6};
		
		for (float i : list) {
			mittelwert += i;
		}
		
		mittelwert = mittelwert / list.length;
		
		System.out.println(mittelwert);
		
		
	}

}
