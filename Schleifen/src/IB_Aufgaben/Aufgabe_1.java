package IB_Aufgaben;

import java.util.Scanner;

public class Aufgabe_1 {

	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		double[] temperatur = new double[24];
		
		//Von 0 bis 23 Uhr sollen die Temperaturen aufgezeichnet werden. Ein neuer Tag beginnt wieder ab 24 => 0 Uhr.
		for(int stunde = 0; stunde <= 23; stunde++) {
			trageWertEin(temperatur, stunde);
		}
		
		
		//Ausgabe der Temperaturen
		for(int i = 0; i < temperatur.length; i++) {
			System.out.printf("Stunde %d: %.2f�C \n", i, temperatur[i]);
		}
		scan.close();
	}
	
	
	//Zu jeder Stunde kann ein Wert eingegeben werden (beispielhaft).
	
	public static void trageWertEin(double[] temperatur, int stunde) {
		System.out.println("Gebe die Temperatur um " + stunde + " Uhr an (�C): ");
		temperatur[stunde] = scan.nextDouble();
	}

}
