package IB_Aufgaben;

import java.util.Scanner;

public class Aufgabe_2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int charCount = 0;
		
		//Eingabe des Zeichens.
		System.out.println("Gib das zu suchende Zeichen ein: ");
		char gesuchtesZeichen = scan.next().charAt(0);
		
		//Eingabe des gewŁnschten Texts.
		System.out.println("Gib den String an, indem das Zeichen gesucht wird:  ");
		String zeichenketteVergleich = scan.next();
		
		
		
		//Vergleich
		for(int idx = 0; idx < zeichenketteVergleich.length(); idx++) {
			if(gesuchtesZeichen == zeichenketteVergleich.charAt(idx)) {
				charCount++;
			}
		}
		
		System.out.printf("Anzahl von %s: %dx", gesuchtesZeichen, charCount);
		
		scan.close();
		
		
	}

}
