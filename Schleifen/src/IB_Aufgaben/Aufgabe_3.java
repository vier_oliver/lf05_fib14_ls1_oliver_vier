package IB_Aufgaben;


public class Aufgabe_3 {

	public static void main(String[] args) {
		
		String[] anArray = new String[20];
		
		//Wie gehe ich vor, wenn zur Laufzeit ein gr��eres Array ben�tigt wird?
		
		//Entweder erstelle ich sofort daf�r eine ArrayList, da diese "dynamisch" bei der Gr��e ist.
		//Andernfalls k�nnte man beim Erreichen des Limits ein neues Array erstellen, welches beispielsweise
		//um 10 gr��er ist und dann die vorherigen Werte einspeichern.
		
		
		
		System.out.println("anArray length bei 10: " + returnArray(anArray, 10).length);
		System.out.println("anArray length bei 19: " + returnArray(anArray, 19).length);
		System.out.println("anArray length bei 20: " + returnArray(anArray, 20).length);
		System.out.println("anArray length bei 30: " + returnArray(anArray, 30).length);
		System.out.println("anArray length bei 40: " + returnArray(anArray, 40).length);
		
	}
	
	public static String[] returnArray(String[] array, int size) {

		for(int i = 0; i < size; i++) {
			
			array[i] = "a";
			
			if(array[array.length-1] != null) {
				String[] tempArray = new String[array.length*2];
				for(int z = 0; z < array.length; z++) {
					tempArray[z] = array[z];
				}
				array = tempArray;
			}
			
		}
			
		return array;

		
	}

}
